const XeroClient = require('xero-node').AccountingAPIClient; //module provided by Xero
    
class Loader
{

    constructor(configId){
        this.config = require(`../config/${configId}/config.json`);
        this.xero = new XeroClient(this.config);
    }
    
    getAccounts(){
        return this.xero.accounts.get(); 
    }
    
    getVendors(){
        return this.xero.contacts.get({ IsSupplier: true});
    }
    
    toJson(obj){
        return JSON.stringify(obj);
    }

}

module.exports = Loader;
