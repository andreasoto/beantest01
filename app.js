"use strict";
const Loader = require('./lib/loader'),
    fs = require('fs');

//Customer's configuration ID 
//ex: node <app.js> <CONFIG_ID>
//Xero API: node app.js demo
const CONFIG_ID = process.argv[2];
const OUTPUT_DIR = `output/${CONFIG_ID}/`;
const TIMESTAMP =  new Date();

(async () => {
    try{
        let loader = new Loader(CONFIG_ID);
        
        const accounts = await loader.getAccounts();
        const jsonAccounts =  loader.toJson(accounts.Accounts);
        
        const vendors = await loader.getVendors();
        let jsonVendors =  loader.toJson(vendors.Contacts);
        
        if (!fs.existsSync(OUTPUT_DIR)) {
            fs.mkdirSync(OUTPUT_DIR);
        }
    
        //write to disk 
        fs.writeFile(`${OUTPUT_DIR}Accounts-${TIMESTAMP}.json`, jsonAccounts, 'utf8', (err, data) => { if(err) process.exit(1);});
        fs.writeFile(`${OUTPUT_DIR}Vendors-${TIMESTAMP}.json`, jsonVendors, 'utf8', (err, data) => { if(err) process.exit(1);});
        
        process.exit(0); //OK!
    }
    catch(error){
        process.exit(1);
    }
    
})();

//Check for errors:
//echo $?