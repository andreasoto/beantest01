const Loader = require('../lib/loader'),
    expect    = require("chai").expect,
    fs = require('fs'),
    assert = require("chai").assert;


describe("Xero API - Demo", function() {
    
    describe("Check configuration", function() {
        it("it should have config.json", function() {
            assert.equal(true,fs.existsSync('./config/demo/config.json'));
        });
    });
    
    describe("Check key", function() {
        it("it should have privatekey.pem", function() {
            assert.equal(true,fs.existsSync('./config/demo/privatekey.pem'));
        });
    });
    
});

describe("Loader Class - Demo", function() {
    
    describe("Check init", function() {
        it("it should return client", function() {
             let loader = new Loader('demo');
             expect(loader).to.be.a('object');
        });
    });
    
    
});