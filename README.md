# BeanTest01
Business requires the integration of their accounting platform to our system.  
For this example,
the goal is to provide the information needed to process Invoices. 

## Business Requirements

There are two mandatory data points needed for any Invoice to be processed which are:  

Vendor (who to pay)  

and the Account (where to pay from)  


## Diagram  

![Diagram](https://bitbucket.org/andreasoto/beantest01/raw/03351aa5ee212945e52641fc6a02a71f899ccfb8/docs/diagram.png)

## Assumptions  

Application stores result on disk as a JSON file. The intention is to load this information into the end system.

Pdf file storage is available but not required in this solution.

Xero Demo Company exist and configuration is completed. 


## Terminology

Vendors = who to pay. Xero Vendors are all 'Contacts' where 'iSupplier' is true  

Account = where to pay from. Xero bank accounts can be found at 'Accounts'

# Application Requirements

node 8+
 
## Installation

clone repository  

 ```
 cd repository
 ``` 
 
 ```
 npm install
 ```

## Running

 ```
 node app.js demo
 ```  
 
node <application> <CONFIG_ID>

## Adding Additional Configs

Create a file in the config folder named '<CONFIG_ID>.json with the following data:

 ```
{
  "appType" : "private",
  "consumerKey": "your_consumer_key",  
  "consumerSecret": "your_secret_key",  
  "privateKeyPath": "./privatekey.pem"
}
 ```  
 
Add privatekey.pem to the same folder.

## Test Configuration 

 ```
 npm test
 ```